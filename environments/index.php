<?php
/**
 * The manifest of files that are local to specific environment.
 * This file returns a list of environments that the application
 * may be installed under. The returned data must be in the following
 * format:
 *
 * ```php
 * return [
 *     'environment name' => [
 *         'path' => 'directory storing the local files',
 *         'skipFiles'  => [
 *             // list of files that should only copied once and skipped if they already exist
 *         ],
 *         'setWritable' => [
 *             // list of directories that should be set writable
 *         ],
 *         'setExecutable' => [
 *             // list of files that should be set executable
 *         ],
 *         'setCookieValidationKey' => [
 *             // list of config files that need to be inserted with automatically generated cookie validation keys
 *         ],
 *         'createSymlink' => [
 *             // list of symlinks to be created. Keys are symlinks, and values are the targets.
 *         ],
 *     ],
 * ];
 * ```
 */
return [
	'Vagrant' => [
        'path' => 'vagrant',
        'setWritable' => [
            '/runtime',
            '/web/assets',
            '/web/uploads',
        ],
        'setExecutable' => [
            'yii',
            'yii_test',
        ],
        'setCookieValidationKey' => [
            '/config/web-local.php',
        ],
    ],
    'Production ukrdev' => [
        'path' => 'prod-ukrdev',
        'setWritable' => [
            '/runtime',
            '/web/assets',
            '/web/uploads',
        ],
        'setExecutable' => [
            'yii',
        ],
        'setCookieValidationKey' => [
            '/config/web-local.php',
        ],
    ],
    'Production customer' => [
        'path' => 'prod-customer',
        'setWritable' => [
            '/runtime',
            '/web/assets',
            '/web/uploads',
        ],
        'setExecutable' => [
            'yii',
        ],
        'setCookieValidationKey' => [
            '/config/web-local.php',
        ],
    ],
    'Production customer [new]' => [
        'path' => 'prod-customer-new',
        'setWritable' => [
            '/runtime',
            '/web/assets',
            '/web/uploads',
        ],
        'setExecutable' => [
            'yii',
        ],
        'setCookieValidationKey' => [
            '/config/web-local.php',
        ],
    ],
];
