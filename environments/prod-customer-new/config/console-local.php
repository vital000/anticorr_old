<?php

$db = require(__DIR__ . '/db-local.php');

$config = [
    'components' => [
        'db' => $db,
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.mail.ru',
                //'username' => 'robotlaw',
                //'password' => 'law2017',
                'username' => 'robot.law@mail.ru',
                'password' => '446367hpQ9H5Xgy',
                //'port' => '465',
                //'encryption' => 'ssl',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;