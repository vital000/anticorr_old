<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";
    }

    public function actionMailTest()
    {
        $mailer = \Yii::$app->get('mailer');
        $message = \Yii::$app->mailer->compose()
            ->setTo('r@ukrdev.com')
            //->setFrom([\Yii::$app->params['adminEmail'] => 'Название сайта'])
            ->setFrom(['robot.law@mail.ru' => 'Название сайта'])
            ->setSubject('Тест')
            ->setTextBody('Тестирование');
        $logger = new \Swift_Plugins_Loggers_ArrayLogger();
        $mailer->getSwiftMailer()->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));
        if (!$message->send()) {
            echo $logger->dump();
        }
    }
}
