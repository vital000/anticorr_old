<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 24.07.2017
 * Time: 16:23
 */

namespace app\assets;


use xj\bootbox\BootboxAsset;
use yii\web\AssetBundle;

class CourseAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/admin/views/default';
    public $js = [
        'course.js',
        'js/scripts.js',
    ];
    public $css = [
        'course.css'
    ];
    public $depends = [
        'app\assets\LodashAsset',
        'app\assets\VueAsset',
        'app\assets\VueResourceAsset',
        'app\assets\VueDraggableAsset',
        'xj\bootbox\BootboxAsset',
        'app\assets\SweetAlertAsset',
        //'app\assets\TinymceAsset',
    ];
    public $publishOptions = [
        'only' => [
            'course.js',
            'course.css',
        ]
    ];
}