<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class SweetAlertAsset
 * @package app\assets
 */
class SweetAlertAsset extends AssetBundle
{
    public $sourcePath = '@bower/sweetalert/dist';
    public $js = [
        'sweetalert.min.js',
    ];
    public $css = [
        'sweetalert.css'
    ];
    public $publishOptions = [
        'only' => [
            'sweetalert.min.js',
            'sweetalert.css',
        ]
    ];
}