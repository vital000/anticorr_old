<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class VueResourceAsset
 * @package common\assets
 */
class VueResourceAsset extends AssetBundle
{
    public $sourcePath = '@bower/vue-resource/dist';
    public $js = [
        'vue-resource.min.js',
    ];
    public $depends = [
        'app\assets\VueAsset'
    ];
    public $publishOptions = [
        'only' => [
            'vue-resource.min.js',
        ]
    ];
}
