<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class VueAsset
 * @package frontend\assets
 */
class LodashAsset extends AssetBundle
{
    public $sourcePath = '@bower/lodash';
    public $js = [
        'lodash.min.js',
    ];
    public $depends = [
        'app\assets\VueAsset',
        'app\assets\SortableJsAsset',
    ];
    public $publishOptions = [
        'only' => [
            'lodash.min.js',
        ]
    ];
}
