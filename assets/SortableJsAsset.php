<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class SortableJsAsset
 * @package frontend\assets
 */
class SortableJsAsset extends AssetBundle
{
    public $sourcePath = '@npm/sortablejs';
    public $js = [
        'Sortable.min.js',
    ];
    public $publishOptions = [
        'only' => [
            'Sortable.min.js',
        ]
    ];
}
