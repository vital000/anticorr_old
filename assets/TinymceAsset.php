<?php

namespace app\assets;

use dosamigos\tinymce\TinyMceLangAsset;
use yii\web\AssetBundle;

/**
 * Class TinymceAsset
 * @package app\assets
 */
class TinymceAsset extends AssetBundle
{
    public $depends = [
        'app\modules\api\assets\TinymceUploadHendlersAsset',
        'dosamigos\tinymce\TinyMceAsset',
        'dosamigos\tinymce\TinyMceLangAsset',
    ];
}