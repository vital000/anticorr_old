<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class VueDraggableAsset
 * @package frontend\assets
 */
class VueDraggableAsset extends AssetBundle
{
    public $sourcePath = '@npm/vuedraggable/dist';
    public $js = [
        'vuedraggable.js',
    ];
    public $publishOptions = [
        'only' => [
            'vuedraggable.js',
        ]
    ];
}
