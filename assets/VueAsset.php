<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class VueAsset
 * @package frontend\assets
 */
class VueAsset extends AssetBundle
{
    public $sourcePath = '@bower/vue/dist';
    public $js = [
        'vue.js',
    ];
    public $publishOptions = [
        'only' => [
            'vue.min.js',
            'vue.js',
        ]
    ];
}
