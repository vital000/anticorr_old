<?php

use yii\db\Migration;

/**
 * Handles adding test_title to table `module`.
 */
class m171119_111956_add_test_title_column_to_module_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%module}}', 'test_title', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%module}}', 'test_title');
    }
}
