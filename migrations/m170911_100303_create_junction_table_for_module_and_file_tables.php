<?php

use yii\db\Migration;

/**
 * Handles the creation of table `module_file`.
 * Has foreign keys to the tables:
 *
 * - `module`
 * - `file`
 */
class m170911_100303_create_junction_table_for_module_and_file_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%module_file}}', [
            'id' => $this->primaryKey(),
            'module_id' => $this->integer()->notNull(),
            'file_id' => $this->integer()->notNull(),
            'name' => $this->string()->null(),
            'type' => $this->smallInteger()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ], $tableOptions);

        // add foreign key for table `module`
        $this->addForeignKey(
            'fk-module_file-module_id',
            '{{%module_file}}', 'module_id',
            '{{%module}}', 'id',
            'CASCADE', 'NO ACTION'
        );

        // add foreign key for table `file`
        $this->addForeignKey(
            'fk-module_file-file_id',
            '{{%module_file}}', 'file_id',
            '{{%file}}', 'id',
            'CASCADE', 'NO ACTION'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%module_file}}');
    }
}
