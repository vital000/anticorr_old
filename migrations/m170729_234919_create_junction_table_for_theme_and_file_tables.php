<?php

use yii\db\Migration;

/**
 * Handles the creation of table `theme_file`.
 * Has foreign keys to the tables:
 *
 * - `theme`
 * - `file`
 */
class m170729_234919_create_junction_table_for_theme_and_file_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%theme_file}}', [
            'id' => $this->primaryKey(),
            'theme_id' => $this->integer()->notNull(),
            'file_id' => $this->integer()->notNull(),
            'name' => $this->string()->null(),
            'type' => $this->smallInteger()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ], $tableOptions);

        // add foreign key for table `theme`
        $this->addForeignKey(
            'fk-theme_file-theme_id',
            '{{%theme_file}}', 'theme_id',
            '{{%theme}}', 'id',
            'CASCADE', 'NO ACTION'
        );

        // add foreign key for table `file`
        $this->addForeignKey(
            'fk-theme_file-file_id',
            '{{%theme_file}}', 'file_id',
            '{{%file}}', 'id',
            'CASCADE', 'NO ACTION'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%theme_file}}');
    }
}
