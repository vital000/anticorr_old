<?php

use yii\db\Migration;

/**
 * Handles adding author to table `module`.
 */
class m170911_111829_add_author_column_to_module_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%module}}', 'author', $this->string()->null());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%module}}', 'author');
    }
}
