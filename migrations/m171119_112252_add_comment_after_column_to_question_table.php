<?php

use yii\db\Migration;

/**
 * Handles adding comment to table `question`.
 */
class m171119_112252_add_comment_after_column_to_question_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%question}}', 'comment_after', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%question}}', 'comment_after');
    }
}
