<?php

use yii\db\Migration;

class m170814_105026_alter_column_description_from_theme_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%theme}}', 'description', 'LONGTEXT DEFAULT NULL');
    }

    public function safeDown()
    {
        $this->alterColumn('{{%theme}}', 'description', $this->text()->null());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170814_105026_alter_column_description_from_theme_table cannot be reverted.\n";

        return false;
    }
    */
}
