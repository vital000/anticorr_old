<?php

use yii\db\Migration;

/**
 * Handles adding msg_after_testing to table `theme`.
 */
class m171117_080257_add_msg_after_testing_column_to_theme_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%theme}}', 'msg_after_testing', $this->text()->null());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%theme}}', 'msg_after_testing');
    }
}
