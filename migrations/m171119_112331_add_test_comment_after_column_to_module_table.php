<?php

use yii\db\Migration;

/**
 * Handles adding comment to table `module`.
 */
class m171119_112331_add_test_comment_after_column_to_module_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%module}}', 'test_comment_after', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%module}}', 'test_comment_after');
    }
}
