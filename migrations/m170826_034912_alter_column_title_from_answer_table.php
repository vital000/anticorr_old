<?php

use yii\db\Migration;

class m170826_034912_alter_column_title_from_answer_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%answer}}', 'title', $this->text()->notNull());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%answer}}', 'title', $this->text()->notNull());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170826_034912_alter_column_title_from_answer_table cannot be reverted.\n";

        return false;
    }
    */
}
