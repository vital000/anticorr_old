<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_done`.
 */
class m170729_030940_create_user_done_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user_done}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'module_id' => $this->integer()->notNull(),
            'theme_id' => $this->integer()->null(),
            'count_try' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk-user_done-user_id',
            '{{%user_done}}', 'user_id',
            '{{%user}}', 'id',
            'CASCADE', 'NO ACTION'
        );

        $this->addForeignKey('fk-user_done-module_id',
            '{{%user_done}}', 'module_id',
            '{{%module}}', 'id',
            'CASCADE', 'NO ACTION'
        );

        $this->addForeignKey('fk-user_done-theme_id',
            '{{%user_done}}', 'theme_id',
            '{{%theme}}', 'id',
            'CASCADE', 'NO ACTION'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%user_done}}');
    }
}
