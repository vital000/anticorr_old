<?php

use yii\db\Migration;

class m170726_154409_create_rbac_base_roles extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;

        // [роль] (студент)
        $student = $auth->createRole('student');
        $student->description = 'Студент';
        $auth->add($student);

        // [роль] (админ)
        $admin = $auth->createRole('admin');
        $admin->description = 'Администратор';
        $auth->add($admin);
        // Разрашим админу все что разрешено студенту
        $auth->addChild($admin, $student);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
    }
}
