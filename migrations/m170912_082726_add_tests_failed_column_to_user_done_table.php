<?php

use yii\db\Migration;

/**
 * Handles adding tests_failed to table `user_done`.
 */
class m170912_082726_add_tests_failed_column_to_user_done_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%user_done}}', 'tests_failed', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%user_done}}', 'tests_failed');
    }
}
