<?php

use yii\db\Migration;

class m171117_065520_add_missing_columns_to_tables_from extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'fio', $this->text());
        $this->addColumn('{{%user}}', 'birthdate', $this->integer(4));
        $this->addColumn('{{%user}}', 'city', $this->text());
        $this->addColumn('{{%user}}', 'job', $this->text());
        $this->addColumn('{{%user}}', 'job_position', $this->text());
        $this->addColumn('{{%user}}', 'groups', $this->integer(5));

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%faq}}', [
            'id' => $this->primaryKey(),
            'question' => $this->text(),
            'answer' => $this->text(),
            'user_id' => $this->integer()->null(),
            'created_at' => $this->integer()->null(),
            'answered_at' => $this->integer()->null(),
            'theme_id' => $this->integer()->null(),
            'subject' => $this->text(),
            'updated_at' => $this->integer()->null(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'fio');
        $this->dropColumn('{{%user}}', 'birthdate');
        $this->dropColumn('{{%user}}', 'city');
        $this->dropColumn('{{%user}}', 'job');
        $this->dropColumn('{{%user}}', 'job_position');
        $this->dropColumn('{{%user}}', 'groups');

        $this->dropTable('{{%faq}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171117_065520_add_missing_columns_to_tables_from cannot be reverted.\n";

        return false;
    }
    */
}
