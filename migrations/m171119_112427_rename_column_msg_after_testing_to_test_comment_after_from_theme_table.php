<?php

use yii\db\Migration;

class m171119_112427_rename_column_msg_after_testing_to_test_comment_after_from_theme_table extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('{{%theme}}', 'msg_after_testing', 'test_comment_after');
    }

    public function safeDown()
    {
        $this->renameColumn('{{%theme}}', 'test_comment_after', 'msg_after_testing');
    }
}
