<?php

use yii\db\Migration;

/**
 * Handles adding test_title to table `theme`.
 */
class m171119_111932_add_test_title_column_to_theme_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%theme}}', 'test_title', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%theme}}', 'test_title');
    }
}
