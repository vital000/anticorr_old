<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_answer`.
 */
class m170728_174907_create_user_answer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user_answer}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'module_id' => $this->integer()->notNull(),
            'theme_id' => $this->integer()->null(),
            'question_id' => $this->integer()->notNull(),
            'answer_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk-user_answer-user_id',
            '{{%user_answer}}', 'user_id',
            '{{%user}}', 'id',
            'CASCADE', 'NO ACTION'
        );

        $this->addForeignKey('fk-user_answer-module_id',
            '{{%user_answer}}', 'module_id',
            '{{%module}}', 'id',
            'CASCADE', 'NO ACTION'
        );

        $this->addForeignKey('fk-user_answer-theme_id',
            '{{%user_answer}}', 'theme_id',
            '{{%theme}}', 'id',
            'CASCADE', 'NO ACTION'
        );

        $this->addForeignKey('fk-user_answer-question_id',
            '{{%user_answer}}', 'question_id',
            '{{%question}}', 'id',
            'CASCADE', 'NO ACTION'
        );

        $this->addForeignKey('fk-user_answer-answer_id',
            '{{%user_answer}}', 'answer_id',
            '{{%answer}}', 'id',
            'CASCADE', 'NO ACTION'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%user_answer}}');
    }
}
