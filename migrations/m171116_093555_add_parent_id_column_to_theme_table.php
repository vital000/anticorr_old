<?php

use yii\db\Migration;

/**
 * Handles adding parent_id to table `theme`.
 */
class m171116_093555_add_parent_id_column_to_theme_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%theme}}', 'parent_id', $this->integer()->null());
        $this->addForeignKey('fk-theme-parent_id'
            , '{{%theme}}', 'parent_id'
            , '{{%theme}}', 'id'
            , 'CASCADE', 'NO ACTION'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-theme-parent_id', '{{%theme}}');
        $this->dropColumn('{{%theme}}', 'parent_id');
    }
}
