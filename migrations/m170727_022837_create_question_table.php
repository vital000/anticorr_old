<?php

use yii\db\Migration;

/**
 * Handles the creation of table `question`.
 */
class m170727_022837_create_question_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%question}}', [
            'id' => $this->primaryKey(),
            'module_id' => $this->integer()->notNull(),
            'theme_id' => $this->integer()->null(),
            'title' => $this->string()->notNull(),
            'sort' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk-question-module_id',
            '{{%question}}', 'module_id',
            '{{%module}}', 'id',
            'CASCADE', 'NO ACTION'
        );

        $this->addForeignKey('fk-question-theme_id',
            '{{%question}}', 'theme_id',
            '{{%theme}}', 'id',
            'CASCADE', 'NO ACTION'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%question}}');
    }
}
