<?php

use yii\db\Migration;

class m170802_020812_alter_title_column_for_question_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%question}}', 'title', $this->text()->notNull());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%question}}', 'title', $this->text()->notNull());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170802_020812_alter_title_column_for_question_table cannot be reverted.\n";

        return false;
    }
    */
}
