<?php

use yii\db\Migration;

/**
 * Handles adding review to table `user`.
 */
class m171117_130802_add_review_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%user}}', 'review', $this->integer()->null());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%user}}', 'review');
    }
}
