<?php

namespace app\fixtures;

use yii\test\ActiveFixture;

class AuthItemChildFixture extends ActiveFixture
{
    public $modelClass = 'app\models\AuthItemChild';
    public $dataFile = '@app/fixtures/data/auth_item_child.php';
    public $depends = [
        'app\fixtures\AuthItemFixture'
    ];
}