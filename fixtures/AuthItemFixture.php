<?php

namespace app\fixtures;

use yii\test\ActiveFixture;

class AuthItemFixture extends ActiveFixture
{
    public $modelClass = 'app\models\AuthItem';
    public $dataFile = '@app/fixtures/data/auth_item.php';
    public $depends = [
        'app\fixtures\AuthRuleFixture'
    ];
}