<?php

namespace app\fixtures;

use yii\test\ActiveFixture;

class AuthRuleFixture extends ActiveFixture
{
    public $modelClass = 'app\models\AuthRule';
    public $dataFile = '@app/fixtures/data/auth_rule.php';
}