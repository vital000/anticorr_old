<?php

return [
    [
        'id' => 1,
        'username' => 'admin',
        'password_hash' => '$2y$13$T7cWDpiT3app5FlOVj7EIOMonEn443rdrRinqI5DDneqPPChTjIey', // admin
        'auth_key' => 'tZsxNcqkPRfnNCGHo_mEYRg4AVZNhEQ_',
        'created_at' => 1501747967,
        'updated_at' => 1501747967,
    ],
];