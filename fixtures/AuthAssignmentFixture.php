<?php

namespace app\fixtures;

use yii\test\ActiveFixture;

class AuthAssignmentFixture extends ActiveFixture
{
    public $modelClass = 'app\models\AuthAssignment';
    public $dataFile = '@app/fixtures/data/auth_assignment.php';
    public $depends = [
        'app\fixtures\AuthItemFixture',
        'app\fixtures\UserFixture'
    ];
}