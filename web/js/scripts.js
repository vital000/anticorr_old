$(document).ready(function(){
    $('#small').on('click',function(){
        $('p').css('font-size',14);
        $('h3').css('font-size',20);
        $('h1').css('font-size',32);
        $('body').css('font-size',14);
        $('.btn-sm, .btn-xs').css('font-size',8);
    });
    $('#medium').on('click',function(){
        $('p').css('font-size',18);
        $('h3').css('font-size',28);
        $('h1').css('font-size',52);
        $('body').css('font-size',18);
        $('.btn-sm, .btn-xs').css('font-size',12);
    });
    $('#big').on('click',function(){
        $('p').css('font-size',26);
        $('h3').css('font-size',36);
        $('h1').css('font-size',70);
        $('body').css('font-size',26);
        $('.btn-sm, .btn-xs').css('font-size',16);
    });


    $('.get_form').children('div').first().addClass('first_questions');
    $('.get_form').children('div').last().addClass('last_questions')
    $(document).on('click', '.input_checkbox', function () {
        if($(this).closest('.well').hasClass('last_questions')){
            if ($(this).closest('span').find('input[type=checkbox]:checked').length) {
                $(this).closest('span').addClass('answered');
            }else{
                $(this).closest('span').removeClass('answered');
            }
            if($('.current .answered').length == $('.current span').length){
                $('.send_answer').show();
            }else{
                $('.send_answer').hide();
            }
        } else{
            if ($(this).closest('span').find('input[type=checkbox]:checked').length) {
                $(this).closest('span').addClass('answered');
                if($('.current .answered').length == $('.current span').length){
                    $('.next').addClass('show_next');
                    $('.next').removeClass('first_questions_button');
                }

            } else{
                $(this).closest('span').removeClass('answered');
                $('.next').removeClass('show_next');
                $('.send_answer').hide();
            }
        }
    });
    $(document).on('click', '.input_radio', function () {
        if($('.current').closest('.well').hasClass('last_questions')){
            $('.send_answer').show();
            $('.next').removeClass('show_next')
        }else{
            if ($('.current').find('input[type=radio]:checked').length) {
                $('.next').addClass('show_next');
            } else{
                $('.next').removeClass('show_next')
            }
        }



    });
    /**
     * Next Button
     **/
        // var  $currDiv = $( ".get_form div:first-child");
    var  $currDiv = $('.get_form').children('div').first();
    $('.next').on('click', function(e) {
        e.preventDefault(e);
        $( "fieldset").removeClass('current');
        $currDiv  = $currDiv .next();
        $( ".get_form > div" ).hide();
        $currDiv .show().find('fieldset').addClass('current');
        $('.prev').addClass('show_prev');
        if(($('.current').find('.checkbox').length && $('.current .answered').length == $('.current span').length) || ($('.current').find('.radio').length && $('.current').find('input[type=radio]:checked').length)){
            $('.next').addClass('show_next');
            if($('.current').closest('.well').hasClass('last_questions')){
                $('.send_answer').show();
                $('.next').removeClass('show_next');
            }
        }else{
            $('.next').removeClass('show_next');
        }
    });
    /**
     * Prev Button
     **/
    $('.prev').click(function(e) {
        e.preventDefault(e);
        $('.send_answer').hide();
        $( "fieldset").removeClass('current');
        $currDiv  = $currDiv .prev();
        $( ".get_form > div" ).hide();
        $currDiv .show().find('fieldset').addClass('current');
        $('.next').addClass('show_next');
        if($('.first_questions').find('.current').length){
            $('.prev').removeClass('show_prev');
        }
    });
});



 function preventSelection(element){
 var preventSelection = false;

 function addHandler(element, event, handler){
 if (element.attachEvent) 
 element.attachEvent('on' + event, handler);
 else 
 if (element.addEventListener) 
 element.addEventListener(event, handler, false);
 }
 function removeSelection(){
 if (window.getSelection) { window.getSelection().removeAllRanges(); }
 else if (document.selection && document.selection.clear)
 document.selection.clear();
 }
 function killCtrlA(event){
 var event = event || window.event;
 var sender = event.target || event.srcElement;
 if (sender.tagName.match(/INPUT|TEXTAREA/i))
 return;
 var key = event.keyCode || event.which;
 if (event.ctrlKey && key == 'A'.charCodeAt(0)) 
 {
 removeSelection();
 if (event.preventDefault) 
 event.preventDefault();
 else
 event.returnValue = false;
 }
 }
 addHandler(element, 'mousemove', function(){
 if(preventSelection)
 removeSelection();
 });
 addHandler(element, 'mousedown', function(event){
 var event = event || window.event;
 var sender = event.target || event.srcElement;
 preventSelection = !sender.tagName.match(/INPUT|TEXTAREA/i);
 });
 addHandler(element, 'mouseup', function(){
 if (preventSelection)
 removeSelection();
 preventSelection = false;
 });
 addHandler(element, 'keydown', killCtrlA);
 addHandler(element, 'keyup', killCtrlA);
 }
 preventSelection(document);
 document.ondragstart = test;
 document.onselectstart = test;
 document.oncontextmenu = test;
 function test() {
 return false
 }